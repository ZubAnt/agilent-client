#-------------------------------------------------
#
# Project created by QtCreator 2017-11-03T14:34:50
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PowerSourceClient
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        src/main/main.cpp \
        src/adapters/QSerialPortAdapter/QSerialPortAdapter.cpp \
        src/main/mainwindow.cpp \
        src/exceptions/OpenPortException.cpp \
        src/exceptions/PortnameException.cpp \
        src/exceptions/ValidateException.cpp \
    src/main/handlers/AdapterSlotHandlers.cpp \
    src/main/setup/SetupAdapterCroupBox.cpp \
    src/senders/QSerialPortSender/QSerialPortSender.cpp \
    src/exceptions/TimeoutException.cpp \
    src/logger/QTextEditLogger.cpp \
    src/main/setup/SetupLogger.cpp \
    src/powerSource/PowerSource.cpp \
    src/listeners/QSerialPortListener.cpp \
    src/main/slots/PublicSlots.cpp \
    src/main/slots/connections.cpp

HEADERS += \
        src/main/mainwindow.h \
        src/exceptions/OpenPortException.h \
        src/exceptions/PortnameException.h \
        src/exceptions/ValidateException.h \
        src/adapters/QSerialPortAdapter/QSerialPortAdapter.h \
    src/senders/QSerialPortSender/QSerialPortSender.h \
    src/exceptions/TimeoutException.h \
    src/logger/QTextEditLogger.h \
    src/powerSource/PowerSource.h \
    src/listeners/QSerialPortListener.h

FORMS += \
        src/main/mainwindow.ui


INCLUDEPATH += \
        src/
        src/main/ \



