#include "main/mainwindow.h"

void MainWindow::_setupBaudRateComboBox()
{
    ui->baudRateComboBox->addItem("9600", QVariant(QSerialPort::Baud9600));
    ui->baudRateComboBox->addItem("1200", QVariant(QSerialPort::Baud1200));
    ui->baudRateComboBox->addItem("2400", QVariant(QSerialPort::Baud2400));
    ui->baudRateComboBox->addItem("4800", QVariant(QSerialPort::Baud4800));
    ui->baudRateComboBox->addItem("19200", QVariant(QSerialPort::Baud19200));
    ui->baudRateComboBox->addItem("38400", QVariant(QSerialPort::Baud38400));
    ui->baudRateComboBox->addItem("57600", QVariant(QSerialPort::Baud57600));
    ui->baudRateComboBox->addItem("115200", QVariant(QSerialPort::Baud115200));

}

void MainWindow::_setupStopBitsComboBox()
{
    ui->stopBitsComboBox->addItem("OneStop", QVariant(QSerialPort::OneStop));
    ui->stopBitsComboBox->addItem("OneAndHalfStop", QVariant(QSerialPort::OneAndHalfStop));
    ui->stopBitsComboBox->addItem("TwoStop", QVariant(QSerialPort::TwoStop));
}

void MainWindow::_setupDataBitsComboBox()
{
    ui->dataBitsComboBox->addItem("8", QVariant(QSerialPort::Data8));
    ui->dataBitsComboBox->addItem("5", QVariant(QSerialPort::Data5));
    ui->dataBitsComboBox->addItem("6", QVariant(QSerialPort::Data6));
    ui->dataBitsComboBox->addItem("7", QVariant(QSerialPort::Data7));
}

void MainWindow::_setupParityComboBox()
{
    ui->parityComboBox->addItem("NoParity", QVariant(QSerialPort::NoParity));
    ui->parityComboBox->addItem("EvenParity", QVariant(QSerialPort::EvenParity));
    ui->parityComboBox->addItem("OddParity", QVariant(QSerialPort::OddParity));
    ui->parityComboBox->addItem("SpaceParity", QVariant(QSerialPort::SpaceParity));
    ui->parityComboBox->addItem("MarkParity", QVariant(QSerialPort::MarkParity));
}

void MainWindow::_setupFlowControlComboBox()
{
    ui->flowControlComboBox->addItem("NoFlowControl", QVariant(QSerialPort::NoFlowControl));
    ui->flowControlComboBox->addItem("HardwareControl", QVariant(QSerialPort::HardwareControl));
    ui->flowControlComboBox->addItem("SoftwareControl", QVariant(QSerialPort::SoftwareControl));
}

void MainWindow::_setupOpenModFlagComboBox()
{
    ui->openModFlagComboBox->addItem("ReadWrite", QVariant(QIODevice::ReadWrite));
    ui->openModFlagComboBox->addItem("WriteOnly", QVariant(QIODevice::WriteOnly));
    ui->openModFlagComboBox->addItem("ReadOnly", QVariant(QIODevice::ReadOnly));
    ui->openModFlagComboBox->addItem("NotOpen", QVariant(QIODevice::NotOpen));
    ui->openModFlagComboBox->addItem("Append", QVariant(QIODevice::Append));
    ui->openModFlagComboBox->addItem("Truncate", QVariant(QIODevice::Truncate));
    ui->openModFlagComboBox->addItem("Text", QVariant(QIODevice::Text));
    ui->openModFlagComboBox->addItem("Unbuffered", QVariant(QIODevice::Unbuffered));
}

void MainWindow::_setupPinoutSignalComboBox()
{
    ui->pinoutSignalComboBox->addItem("NoSignal", QVariant(QSerialPort::NoSignal));
    ui->pinoutSignalComboBox->addItem("TransmittedDataSignal", QVariant(QSerialPort::TransmittedDataSignal));
    ui->pinoutSignalComboBox->addItem("ReceivedDataSignal", QVariant(QSerialPort::ReceivedDataSignal));
    ui->pinoutSignalComboBox->addItem("DataTerminalReadySignal", QVariant(QSerialPort::DataTerminalReadySignal));
    ui->pinoutSignalComboBox->addItem("DataCarrierDetectSignal", QVariant(QSerialPort::DataCarrierDetectSignal));
    ui->pinoutSignalComboBox->addItem("DataSetReadySignal", QVariant(QSerialPort::DataSetReadySignal));
    ui->pinoutSignalComboBox->addItem("RingIndicatorSignal", QVariant(QSerialPort::RingIndicatorSignal));
    ui->pinoutSignalComboBox->addItem("RequestToSendSignal", QVariant(QSerialPort::RequestToSendSignal));
    ui->pinoutSignalComboBox->addItem("ClearToSendSignal", QVariant(QSerialPort::ClearToSendSignal));
    ui->pinoutSignalComboBox->addItem("SecondaryTransmittedDataSignal", QVariant(QSerialPort::SecondaryTransmittedDataSignal));
    ui->pinoutSignalComboBox->addItem("SecondaryReceivedDataSignal", QVariant(QSerialPort::SecondaryReceivedDataSignal));
}
