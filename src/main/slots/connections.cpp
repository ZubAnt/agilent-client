#include "ui_mainwindow.h"
#include "main/mainwindow.h"

void MainWindow::_setupSlotsConnections()
{
    QObject::connect(_qSerialPortListener, SIGNAL(currentReady(QString)),
                     this, SLOT(displayCurrent(QString)));

    QObject::connect(_qSerialPortListener, SIGNAL(voltageReady(QString)),
                     this, SLOT(displayVoltage(QString)));

    QObject::connect(_qSerialPortListener, SIGNAL(qSerialPortSenderReady(QString)),
                     this, SLOT(displayQSerialPortSenderAnswer(QString)));
}
