#include "ui_mainwindow.h"
#include "main/mainwindow.h"

#include "exceptions/OpenPortException.h"
#include "exceptions/PortnameException.h"

void MainWindow::on_adapterScanPushButton_clicked()
{
    ui->availablePortsCheckBox->clear();
    if (_qSerialPortAdapter->isOpen())
    {
        ui->availablePortsCheckBox->addItem(_qSerialPortAdapter->portName());
    }
    Q_FOREACH(QSerialPortInfo port, _qSerialPortAdapter->availablePorts())
    {
        ui->availablePortsCheckBox->addItem(port.portName());
    }
}

void MainWindow::on_adapterClosePushButton_clicked()
{
    _qSerialPortAdapter->close();
}

void MainWindow::on_adapterOpenPushButton_clicked()
{
    int countAvailablePorts = ui->availablePortsCheckBox->count();
    if (!countAvailablePorts)
    {
        _qSerialPortAdapter->logger()->error("Not available COM ports");
        return;
    }

    QString portName = ui->availablePortsCheckBox->currentText();
//    QString portName = "1";
    QString baudRateName = ui->baudRateComboBox->currentText();
    QString stopBitsName = ui->stopBitsComboBox->currentText();
    QString dataBitsName = ui->dataBitsComboBox->currentText();
    QString parityName = ui->parityComboBox->currentText();
    QString flowControlName = ui->flowControlComboBox->currentText();
    QString openModFlagName = ui->openModFlagComboBox->currentText();
    QString pinoutSignalName = ui->pinoutSignalComboBox->currentText();


    QString msg = "Try open port with next parameters:\n";
    msg += "   - portName: " + portName + "\n";
    msg += "   - baudRateName: " + baudRateName + "\n";
    msg += "   - stopBitsName: " + stopBitsName + "\n";
    msg += "   - dataBitsName: " + dataBitsName + "\n";
    msg += "   - parityName: " + parityName + "\n";
    msg += "   - flowControlName: " + flowControlName + "\n";
    msg += "   - openModFlagName: " + openModFlagName + "\n";
    msg += "   - pinoutSignalName: " + pinoutSignalName;

    _qSerialPortAdapter->logger()->debug(msg);


//    _qSerialPortAdapter->setPortName(portName);
//    _qSerialPortAdapter->setBaudRateName(baudRateName);
//    _qSerialPortAdapter->setStopBitsName(stopBitsName);
//    _qSerialPortAdapter->setDataBitsName(dataBitsName);
//    _qSerialPortAdapter->setParityName(parityName);
//    _qSerialPortAdapter->setFlowControlName(flowControlName);
//    _qSerialPortAdapter->setOpenModeFlagName(openModFlagName);

    _qSerialPortAdapter->setPortName(portName);
    _qSerialPortAdapter->setBaudRate((QSerialPort::BaudRate) ui->baudRateComboBox->currentData().toInt());
    _qSerialPortAdapter->setStopBits((QSerialPort::StopBits) ui->stopBitsComboBox->currentData().toInt());
    _qSerialPortAdapter->setDataBits((QSerialPort::DataBits) ui->dataBitsComboBox->currentData().toInt());
    _qSerialPortAdapter->setParity((QSerialPort::Parity) ui->parityComboBox->currentData().toInt());
    _qSerialPortAdapter->setFlowControl((QSerialPort::FlowControl) ui->flowControlComboBox->currentData().toInt());
    _qSerialPortAdapter->setOpenModeFlag((QIODevice::OpenModeFlag) ui->openModFlagComboBox->currentData().toInt());


    QVariant variant = ui->stopBitsComboBox->currentData();

    try
    {
        _qSerialPortAdapter->open();
    }
    catch (OpenPortException &err)
    {
        QString msg("Can't open port: ");
        msg += err.what();
         _qSerialPortAdapter->logger()->error(msg);
    }
    catch (PortNameException &err)
    {
        QString msg("[ERROR] Can't open port: ");
        msg += err.what();
        _qSerialPortAdapter->logger()->error(msg);
    }
}
