#include "OpenPortException.h"

OpenPortException::OpenPortException():
    logic_error("can't open port")
{}

OpenPortException::OpenPortException(const std::string &msg):
    logic_error(msg)
{}

OpenPortException::OpenPortException(const char *msg):
    logic_error(msg)
{}
