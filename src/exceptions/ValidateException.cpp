#include "ValidateException.h"

ValidateException::ValidateException():
    invalid_argument("can't validate")
{}

ValidateException::ValidateException(const std::string &msg):
    invalid_argument(msg)
{}

ValidateException::ValidateException(const char *msg):
    invalid_argument(msg)
{}
