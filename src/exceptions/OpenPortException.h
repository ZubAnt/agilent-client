#ifndef OPEN_PORT_EXCEPTION_H
#define OPEN_PORT_EXCEPTION_H

#include <stdexcept>

class OpenPortException: public std::logic_error
{
public:
    OpenPortException();
    OpenPortException(const std::string &msg);
    OpenPortException(const char *msg);
};

#endif // OPEN_PORT_EXCEPTION_H
