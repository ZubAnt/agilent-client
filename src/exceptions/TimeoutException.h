#ifndef TIMEOUTECXEPTION_H
#define TIMEOUTECXEPTION_H

#include <stdexcept>

class TimeoutException: public std::runtime_error
{
public:
    TimeoutException();
    TimeoutException(const std::string &msg);
    TimeoutException(const char *msg);
};

#endif // TIMEOUTECXEPTION_H
