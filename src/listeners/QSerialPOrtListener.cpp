#include "QSerialPortListener.h"
#include "QDebug"

#include <exceptions/OpenPortException.h>
#include <exceptions/TimeoutException.h>

void QSerialPortListener::run()
{
    while (true) {

        if (_isRunning) {
            if (_sender->isReadyRead()) {
                qDebug() << "sender is ready for read";
                QString answer;
                while (_sender->isReadyRead()) {
                    try {
                        answer += _sender->read();
                    } catch (OpenPortException &err) {
                        qDebug() << err.what();
                    } catch (TimeoutException &err) {
                        qDebug() << err.what();
                    }
                }

                if (_waitAnswerType == WaitAnswerType::Current) {
                    qDebug() << "try emit current";
                    emit currentReady(answer);
                } else if (_waitAnswerType == WaitAnswerType::Voltage) {
                    qDebug() << "try emit voltage";
                    emit voltageReady(answer);
                } else if (_waitAnswerType == WaitAnswerType::QSerialPortSenderAnswer) {
                    qDebug() << "try emit answer for QSerialProtSender";
                    emit qSerialPortSenderReady(answer);
                }

                _waitAnswerType = WaitAnswerType::None;
                qDebug() << "Answer = " + answer;
            }
        }
    }
}

QSerialPortListener::QSerialPortListener(QSerialPortSender *sender):
    _sender(sender),
    _isRunning(true)
{}

void QSerialPortListener::setWaitAnswerType(quint32 type)
{
    _waitAnswerType = (WaitAnswerType) type;
}

bool QSerialPortListener::isRunning() const
{
    return _isRunning;
}

void QSerialPortListener::lock()
{
    _isRunning = false;
}

void QSerialPortListener::unlock()
{
    _isRunning = true;
}
