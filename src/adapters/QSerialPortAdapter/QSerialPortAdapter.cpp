#include "QSerialPortAdapter.h"
#include <QSerialPort>
#include "exceptions/PortnameException.h"
#include "exceptions/ValidateException.h"
#include "exceptions/OpenPortException.h"
#include <QVariant>
#include <QDebug>
#include <exceptions/TimeoutException.h>

QSerialPortAdapter::QSerialPortAdapter(QObject *parent, QTextEdit *loggerEditor)
{
    _serialPort = new QSerialPort(parent);
    _logger = new QTextEditLogger("Adapter", loggerEditor);
}

QSerialPortAdapter::~QSerialPortAdapter()
{
    delete _serialPort;
}

QList<QSerialPortInfo> QSerialPortAdapter::availablePorts()
{
    return QSerialPortInfo::availablePorts();
}

void QSerialPortAdapter::open()
{

    if (_serialPort->isOpen()) {
        QString msg = "Port now is open on " + _serialPort->portName() + " port";
        throw OpenPortException(msg.toStdString());
    }
    if (_portName.isEmpty()) {  throw PortNameException("Empty port name"); }

//    QSerialPort::BaudRate baudRate = _toBaudRate(_baudRateName);
//    QSerialPort::Parity parity = _toParity(_parityName);
//    QSerialPort::StopBits stopBits = _toStopBits(_stopBitsName);
//    QSerialPort::DataBits databits = _toDataBits(_dataBitsName);
//    QSerialPort::FlowControl flowControl = _toFlowControl(_flowControlName);
//    QIODevice::OpenModeFlag openModeFlag = _toOpenModeFlag(_openModeFlagName);
//    _validate(baudRate, parity, stopBits, databits, flowControl, openModeFlag);

    _serialPort->setPortName(_portName);
    _serialPort->setBaudRate(_baudRate);
    _serialPort->setParity(_parity);
    _serialPort->setStopBits(_stopBits);
    _serialPort->setDataBits(_dataBits);
    _serialPort->setFlowControl(_flowControl);

    bool ok = _serialPort->open(_openModeFlag);
    if (!ok) { throw OpenPortException("bad current settings"); }
    _serialPort->clear(QSerialPort::AllDirections);
}

void QSerialPortAdapter::close()
{
    _serialPort->close();
}

bool QSerialPortAdapter::isOpen()
{
    return _serialPort->isOpen();
}

QString QSerialPortAdapter::portName() const
{
    return _portName;
}

void QSerialPortAdapter::setPortName(const QString &portName)
{
    _portName = portName;
}

QString QSerialPortAdapter::baudRateName() const
{
    return _baudRateName;
}

void QSerialPortAdapter::setBaudRateName(const QString &baudRateName)
{
    _baudRateName = baudRateName;
}

QString QSerialPortAdapter::dataBitsName() const
{
    return _dataBitsName;
}

void QSerialPortAdapter::setDataBitsName(const QString &dataBitsName)
{
    _dataBitsName = dataBitsName;
}

QString QSerialPortAdapter::stopBitsName() const
{
    return _stopBitsName;
}

void QSerialPortAdapter::setStopBitsName(const QString &stopBitsName)
{
    _stopBitsName = stopBitsName;
}

QString QSerialPortAdapter::parityName() const
{
    return _parityName;
}

void QSerialPortAdapter::setParityName(const QString &parityName)
{
    _parityName = parityName;
}

QString QSerialPortAdapter::flowControlName() const
{
    return _flowControlName;
}

void QSerialPortAdapter::setFlowControlName(const QString &flowControlName)
{
    _flowControlName = flowControlName;
}

QString QSerialPortAdapter::openModeFlagName() const
{
    return _openModeFlagName;
}

void QSerialPortAdapter::setOpenModeFlagName(const QString &openModeFlagName)
{
    _openModeFlagName = openModeFlagName;
}

QSerialPort::BaudRate QSerialPortAdapter::baudRate() const
{
    return _baudRate;
}

void QSerialPortAdapter::setBaudRate(const QSerialPort::BaudRate &baudRate)
{
    _baudRate = baudRate;
}

QSerialPort::DataBits QSerialPortAdapter::dataBits() const
{
    return _dataBits;
}

void QSerialPortAdapter::setDataBits(const QSerialPort::DataBits &dataBits)
{
    _dataBits = dataBits;
}

QSerialPort::StopBits QSerialPortAdapter::stopBits() const
{
    return _stopBits;
}

void QSerialPortAdapter::setStopBits(const QSerialPort::StopBits &stopBits)
{
    _stopBits = stopBits;
}

QSerialPort::Parity QSerialPortAdapter::parity() const
{
    return _parity;
}

void QSerialPortAdapter::setParity(const QSerialPort::Parity &parity)
{
    _parity = parity;
}

QSerialPort::FlowControl QSerialPortAdapter::flowControl() const
{
    return _flowControl;
}

void QSerialPortAdapter::setFlowControl(const QSerialPort::FlowControl &flowControl)
{
    _flowControl = flowControl;
}

QSerialPort::PinoutSignal QSerialPortAdapter::pinoutSignal() const
{
    return _pinoutSignal;
}

void QSerialPortAdapter::setPinoutSignal(const QSerialPort::PinoutSignal &pinoutSignal)
{
    _pinoutSignal = pinoutSignal;
}

QIODevice::OpenModeFlag QSerialPortAdapter::openModeFlag() const
{
    return _openModeFlag;
}

void QSerialPortAdapter::setOpenModeFlag(const QIODevice::OpenModeFlag &openModeFlag)
{
    _openModeFlag = openModeFlag;
}

void QSerialPortAdapter::write(const QByteArray &data, int writeTimout)
{
    _serialPort->write(data);
    if(!_serialPort->waitForBytesWritten(writeTimout)) { throw TimeoutException("write timeout"); }
}

QByteArray QSerialPortAdapter::read(int readTimeout)
{
    QByteArray response = _serialPort->readAll();
    if (!_serialPort->waitForReadyRead(readTimeout)) { throw TimeoutException("read timeout"); }
    return response;
}

QTextEditLogger *QSerialPortAdapter::logger() const
{
    return _logger;
}

bool QSerialPortAdapter::isReadyRead()
{
    return _serialPort->bytesAvailable() > 0;
//    return _serialPort->isReadable();
}


QSerialPort::Parity QSerialPortAdapter::_toParity(const QString &str) const
{
    return _parityMap.value(str, QSerialPort::UnknownParity);
}

QSerialPort::BaudRate QSerialPortAdapter::_toBaudRate(const QString &str) const
{
    return _baudRateMap.value(str, QSerialPort::UnknownBaud);
}

QSerialPort::DataBits QSerialPortAdapter::_toDataBits(const QString &str) const
{
    return _dataBitsMap.value(str, QSerialPort::UnknownDataBits);
}

QSerialPort::StopBits QSerialPortAdapter::_toStopBits(const QString &str) const
{
    return _stopBitsMap.value(str, QSerialPort::UnknownStopBits);
}

QSerialPort::FlowControl QSerialPortAdapter::_toFlowControl(const QString &str) const
{
    return _flowControlMap.value(str, QSerialPort::UnknownFlowControl);
}

QSerialPort::PinoutSignal QSerialPortAdapter::_toPinoutSignal(const QString &str) const
{
    return _pinoutSignalMap.value(str, QSerialPort::NoSignal);
}

QIODevice::OpenModeFlag QSerialPortAdapter::_toOpenModeFlag(const QString &str) const
{
    return _openModFlagMap.value(str, QIODevice::NotOpen);
}

void QSerialPortAdapter::_validate(QSerialPort::BaudRate baudRate, QSerialPort::Parity parity,
                                  QSerialPort::StopBits stopBits, QSerialPort::DataBits dataBits,
                                  QSerialPort::FlowControl flowControl, QIODevice::OpenModeFlag openModeFlag)
{
    if (baudRate == QSerialPort::UnknownBaud) { throw ValidateException("[QSerialPortAdapter._validate] Unknown BaudRate"); }
    if (parity == QSerialPort::UnknownParity) { throw ValidateException("[QSerialPortAdapter._validate] Unknown Parity"); }
    if (stopBits == QSerialPort::UnknownStopBits) { throw ValidateException("[QSerialPortAdapter._validate] Unknown StopBits"); }
    if (dataBits == QSerialPort::UnknownDataBits) { throw ValidateException("[QSerialPortAdapter._validate] Unknown DataBits"); }
    if (flowControl == QSerialPort::UnknownFlowControl) { throw ValidateException("[QSerialPortAdapter._validate] Unknown FlowControl"); }
    if (openModeFlag == QIODevice::NotOpen) { throw ValidateException("[QSerialPortAdapter._validate] OpenModeFlag is NotOpen"); }
}
