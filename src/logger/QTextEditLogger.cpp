#include "QTextEditLogger.h"
#include <QDateTime>
#include <QDebug>

QTextEditLogger::QTextEditLogger(const QString &author, QTextEdit *editor, qint32 level):
    _author(author),
    _editor(editor),
    _level((Level) level)
{}

QTextEditLogger::~QTextEditLogger()
{}

void QTextEditLogger::info(QString msg) const
{
    if(!_editor) { return; }
    if (Level::INFO >= _level) {
      _editor->append(_buid(msg, Level::INFO));
    };
}

void QTextEditLogger::warn(QString msg) const
{
    if(!_editor) { return; }
    if (Level::WARN >= _level) {
      _editor->append(_buid(msg, Level::WARN));
    };
}

void QTextEditLogger::debug(QString msg) const
{
    if(!_editor) { return; }
    if (Level::DEBUG >= _level) {
      _editor->append(_buid(msg, Level::DEBUG));
    };
}

void QTextEditLogger::error(QString msg) const
{
    if(!_editor) { return; }
    if (Level::ERROR >= _level) {
        QString rsp = _buid(msg, Level::ERROR);
      _editor->append(rsp);
    };
}

void QTextEditLogger::setLevel(const qint32 level)
{
     _level = (Level) level;
}

QString QTextEditLogger::_buid(const QString &msg, Level level) const
{
    QString response;
    QDateTime now = QDateTime::currentDateTime();
    response += "[" + now.toString(Qt::DateFormat::SystemLocaleShortDate) + "] " ;
    response += _author + " " + _levelMap.value(level) + ": " + msg;
    return response;
}
